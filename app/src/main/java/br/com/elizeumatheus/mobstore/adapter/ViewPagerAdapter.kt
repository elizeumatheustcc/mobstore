package br.com.elizeumatheus.mobstore.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import br.com.elizeumatheus.mobstore.fragments.CadastroFragment
import br.com.elizeumatheus.mobstore.fragments.LoginFragment

class ViewPagerAdapter : FragmentPagerAdapter {


    private var mFragmentTitleList : MutableList<String> = mutableListOf("Login", "Cadastre-se")
    private var mFragmentTags : MutableMap<Int, String> = mutableMapOf()
    private var mFragmentManager  : FragmentManager? = null

    constructor(manager: FragmentManager) : super(manager){
        this.mFragmentManager = manager
    }

    override fun getItem(position: Int): Fragment {
        return if(position == 0){
            LoginFragment()
        }else{
            CadastroFragment()
        }
    }

    override fun getCount(): Int {
       return NUM_ITEMS
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val item = super.instantiateItem(container, position)
        if(item is Fragment){
            val tag = item.tag
            mFragmentTags[position] = tag!!
        }
        return item
    }

    fun getFragment(position : Int) : Fragment?{
        var fragment : Fragment? = null
        var tag : String? = mFragmentTags[position]
        if(tag != null){
            fragment = mFragmentManager?.findFragmentByTag(tag)
        }
        return fragment
    }

    companion object {
        private const val NUM_ITEMS = 2
    }


}