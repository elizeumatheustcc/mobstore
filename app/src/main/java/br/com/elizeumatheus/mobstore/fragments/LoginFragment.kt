package br.com.elizeumatheus.mobstore.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import br.com.elizeumatheus.mobstore.LoginActivity
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.databinding.FragmentLoginBinding
import br.com.elizeumatheus.mobstore.viewmodels.LoginViewModel
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment(){

    private var firebaseAuth : FirebaseAuth? = null
    private lateinit var viewModel : LoginViewModel

    private var fragmentInterfaceListener : LoginFragmentInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding : FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        binding.model = viewModel

        firebaseAuth = FirebaseAuth.getInstance()

        val scrollView = binding.root.findViewById<ScrollView>(R.id.loginScrollView)
        val progressBar = binding.root.findViewById<ProgressBar>(R.id.loginProgressBar)

        val awesomeValidation = AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT)

        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.loginTextInputLayoutUsuario), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.loginTextInputLayoutSenha), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))

        binding.root.findViewById<Button>(R.id.loginBtnContinuar)?.setOnClickListener{

            if(awesomeValidation.validate()){

                scrollView.visibility = View.GONE
                progressBar.visibility = View.VISIBLE

                awesomeValidation.clear()

                val usuario = binding.root.findViewById<TextView>(R.id.loginEditTextUsuario)
                val senha = binding.root.findViewById<TextView>(R.id.loginEditTextSenha)

                firebaseAuth?.signInWithEmailAndPassword(usuario.text.toString(), senha.text.toString())!!.addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        scrollView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE

                        fragmentInterfaceListener?.onTerminoLogin()
                    } else {

                        scrollView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE

                        Toast.makeText(activity, "Usuário e/ou senha incorretos", Toast.LENGTH_LONG).show()
                    }
                }

            }

        }

        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is LoginActivity){
            fragmentInterfaceListener = context
        }
    }

    interface LoginFragmentInterface{
        fun onTerminoLogin()
    }

    companion object {
        const val TAG = "LOGIN_FRAGMENT"
    }

}