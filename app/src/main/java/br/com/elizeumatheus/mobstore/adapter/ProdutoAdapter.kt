package br.com.elizeumatheus.mobstore.adapter

import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.utils.AdapterCallback
import br.com.elizeumatheus.mobstore.utils.RecyclerItemClickListener
import br.com.elizeumatheus.mobstore.viewmodels.ProdutoViewModel
import com.bumptech.glide.Glide

class ProdutoAdapter(var fragment: Fragment, var items: MutableList<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listener : AdapterCallback = fragment as AdapterCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            LISTA -> {
                view = inflater.inflate(R.layout.layout_produto_card, parent, false)
                viewHolder = ListaViewHolder(view)
            }
            else -> {
                view = inflater.inflate(R.layout.layout_produto_header, parent, false)
                viewHolder = HeaderViewHolder(view)
            }
        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            LISTA -> {
                val produtos = (items[position] as List<*>).filterIsInstance<ProdutoViewModel>()
                val viewHolder = holder as ListaViewHolder

                viewHolder.recyclerView.setHasFixedSize(true)

                val layoutManager = LinearLayoutManager(holder.recyclerView.context)
                layoutManager.orientation = LinearLayoutManager.HORIZONTAL
                viewHolder.recyclerView.layoutManager = layoutManager

                val adapter = ProdutoItemAdapter(viewHolder.recyclerView.context, produtos)
                viewHolder.recyclerView.adapter = adapter

                viewHolder.recyclerView.addOnItemTouchListener(RecyclerItemClickListener(fragment.context!!, holder.recyclerView, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val id = produtos[position].id
                        val categoria = produtos[position].categoria
                        listener.onItemClick(id, categoria)
                    }

                    override fun onLongItemClick(view: View, position: Int) {

                    }
                }))

            }
            HEADER -> {
                val produto = items[position] as ProdutoViewModel
                val viewHolder = holder as HeaderViewHolder

                Glide.with(fragment).load(produto.imagem).into(viewHolder.imagem)
                viewHolder.titulo.text = produto.nome
                viewHolder.preco.text = produto.preco

                viewHolder.comprar.setOnClickListener{
                    listener.onComprar(produto.id)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (items[position] is List<*>) {
            LISTA
        } else {
            HEADER
        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imagem: ImageView = itemView.findViewById(R.id.produtoHeaderImageViewImagem)
        var titulo: TextView = itemView.findViewById(R.id.produtoHeaderTextViewTitulo)
        var preco: TextView = itemView.findViewById(R.id.produtoHeaderTextViewPreco)
        var comprar: Button = itemView.findViewById(R.id.produtoHeaderButtonComprar)

    }

    class ListaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var recyclerView: RecyclerView = itemView.findViewById(R.id.produtoCardRecyclerView)
    }

    companion object {
        const val HEADER = 0
        const val LISTA = 1
    }

}