package br.com.elizeumatheus.mobstore.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.viewmodels.CategoriaViewModel


class CategoriaAdapter(private var context : Context, private var categorias : MutableList<CategoriaViewModel>) : BaseAdapter() {

    private class ViewHolder(view : View?){
        var icone : ImageView? = null
        var descricao : TextView? = null

        init {
            this.icone = view?.findViewById(R.id.listItemCategoriasImageViewIcone)
            this.descricao = view?.findViewById(R.id.listItemCategoriasTextViewDescricao)
        }
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view : View?
        val viewHolder : ViewHolder

        if(convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item_categorias, parent, false)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        }else{
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        val categoria = categorias[position]

        val icone = context.resources.getIdentifier(categoria.Icone, "drawable", context.packageName)
        val drawable = ContextCompat.getDrawable(context, icone)

        viewHolder.icone?.setImageDrawable(drawable)
        viewHolder.descricao?.text = categoria.Descricao

        return view as View
    }

    override fun getItem(position: Int): Any {
        return categorias[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return categorias.size
    }

}