package br.com.elizeumatheus.mobstore.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import br.com.elizeumatheus.mobstore.LoginActivity
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.databinding.FragmentCadastroBinding
import br.com.elizeumatheus.mobstore.viewmodels.CadastroViewModel
import br.com.elizeumatheus.mobstore.viewmodels.UsuarioViewModel
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class CadastroFragment : Fragment() {

    private var firebaseAuth: FirebaseAuth? = null
    private var reference: DatabaseReference? = null
    private lateinit var viewModel: CadastroViewModel

    private var fragmentInterfaceListener : CadastroFragmentInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentCadastroBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cadastro, container, false)

        viewModel = ViewModelProviders.of(this).get(CadastroViewModel::class.java)

        binding.model = viewModel

        firebaseAuth = FirebaseAuth.getInstance()
        reference = FirebaseDatabase.getInstance().reference


        val scrollView = binding.root.findViewById<ScrollView>(R.id.cadastroScrollView)
        val progressBar = binding.root.findViewById<ProgressBar>(R.id.cadastroProgressBar)

        val awesomeValidation = AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT)

        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutNome), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutUsuario), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutUsuario), android.util.Patterns.EMAIL_ADDRESS, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutSenha), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutConfirmarSenha), RegexTemplate.NOT_EMPTY, getString(R.string.error_campo_obrigatorio))
        awesomeValidation.addValidation(binding.root.findViewById(R.id.cadastroTextInputLayoutConfirmarSenha), binding.root.findViewById<TextInputLayout>(R.id.cadastroTextInputLayoutSenha), getString(R.string.error_senhas_nao_conferem))


        binding.root.findViewById<Button>(R.id.cadastroBtnContinuar)?.setOnClickListener {

            if (awesomeValidation.validate()) {

                scrollView.visibility = View.GONE
                progressBar.visibility = View.VISIBLE

                awesomeValidation.clear()

                val nome = binding.root.findViewById<TextView>(R.id.cadastroEditTextNome)
                val login = binding.root.findViewById<TextView>(R.id.cadastroEditTextUsuario)
                val senha = binding.root.findViewById<TextView>(R.id.cadastroEditTextSenha)

                firebaseAuth?.createUserWithEmailAndPassword(login.text.toString(), senha.text.toString())!!
                        .addOnCompleteListener { task ->

                            if (task.isSuccessful) {

                                val user = task.result.user

                                val usuario = UsuarioViewModel(user.uid, nome.text.toString(), login.text.toString())
                                reference?.child("usuarios")!!.child(user.uid).setValue(usuario)

                                scrollView.visibility = View.VISIBLE
                                progressBar.visibility = View.GONE

                                fragmentInterfaceListener?.onTerminoCadastro(nome.text.toString())

                            } else {

                                scrollView.visibility = View.VISIBLE
                                progressBar.visibility = View.GONE

                                Toast.makeText(activity, getString(R.string.error_falha_conexao), Toast.LENGTH_LONG).show()
                            }
                        }

            }

        }



        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is LoginActivity){
            fragmentInterfaceListener = context
        }
    }

    interface CadastroFragmentInterface{
        fun onTerminoCadastro(nome : String)
    }

    companion object {
        const val TAG = "CADASTRO_FRAGMENT"
    }

}