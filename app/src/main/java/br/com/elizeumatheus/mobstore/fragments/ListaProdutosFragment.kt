package br.com.elizeumatheus.mobstore.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.ProgressBar
import br.com.elizeumatheus.mobstore.ProdutosActivity
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.adapter.ListaProdutosAdapter
import br.com.elizeumatheus.mobstore.viewmodels.ListaProdutosViewModel
import com.google.firebase.database.*

class ListaProdutosFragment : Fragment(){

    private var database : FirebaseDatabase = FirebaseDatabase.getInstance()
    private var myRef : DatabaseReference = database.reference
    private var listaProdutos : MutableList<ListaProdutosViewModel> = mutableListOf()

    private var fragmentInterfaceListener : ListaProdutosFragmentInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val layout = inflater.inflate(R.layout.fragment_lista_produtos, container, false)

        val idCategoria = arguments?.getInt("idCategoria")
        val nomeProduto = arguments?.getString("nomeProduto")

        val listView = layout.findViewById<ListView>(R.id.listaProdutoListView)
        val progressBar = layout.findViewById<ProgressBar>(R.id.listaProdutoProgressBar)

        listView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE

        var query : Query? = null

        if(idCategoria != 0){
            if (idCategoria != null) {
                query = myRef.child("produtos").orderByChild("categoria").equalTo(idCategoria.toDouble())
            }
        }

        query?.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.exists()){
                    for(postSnapshot : DataSnapshot in dataSnapshot.children){
                       val produto : ListaProdutosViewModel = postSnapshot.getValue(ListaProdutosViewModel::class.java)!!
                        listaProdutos.add(produto)
                    }

                    val adapter = ListaProdutosAdapter(context!!, listaProdutos)
                    listView.adapter = adapter

                    adapter.notifyDataSetChanged()

                    listView?.visibility = View.VISIBLE
                    progressBar?.visibility = View.GONE

                }
            }
        })

        listView.setOnItemClickListener {
            adapterView : AdapterView<*>, _, position, _ ->

            val produto = adapterView.adapter.getItem(position) as ListaProdutosViewModel
            fragmentInterfaceListener?.onSelecionarProduto(produto.id, idCategoria!!)

        }


        return layout
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is ProdutosActivity){
            fragmentInterfaceListener = context
        }
    }

    interface ListaProdutosFragmentInterface{
        fun onSelecionarProduto(id : Int, categoria : Int)
    }

    companion object {
        const val TAG = "LISTA_PRODUTOS_FRAGMENT"

        fun newInstance(idCategoria : Int = 0, nomeProduto : String?) : ListaProdutosFragment{
            val fragment = ListaProdutosFragment()

            val args = Bundle()
            args.putInt("idCategoria", idCategoria)
            args.putString("nomeProduto", nomeProduto)

            fragment.arguments = args

            return fragment
        }
    }

}