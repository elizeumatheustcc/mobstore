package br.com.elizeumatheus.mobstore.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.viewmodels.ItemViewModel
import br.com.elizeumatheus.mobstore.viewmodels.KItemViewModel

import com.bumptech.glide.Glide

class ItemAdapter(var context: Context, private var items: List<KItemViewModel>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
        val itemViewLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)

        return ViewHolder(itemViewLayout)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemAdapter.ViewHolder, position: Int) {

        val item = items[position]

        holder.titulo.text = item.nome
        holder.preco.text = item.preco

        Glide.with(context).load(item.imagem).into(holder.imagem)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var titulo: TextView
        var preco: TextView
        var imagem: ImageView

        init {
            titulo = itemView.findViewById(R.id.itemTextViewTitulo)
            preco = itemView.findViewById(R.id.itemTextViewPreco)
            imagem = itemView.findViewById(R.id.itemImageViewImagem)
        }

    }


}