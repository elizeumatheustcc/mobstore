package br.com.elizeumatheus.mobstore

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.widget.Toast
import br.com.elizeumatheus.mobstore.fragments.ListaProdutosFragment
import br.com.elizeumatheus.mobstore.fragments.ProdutoFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class ProdutosActivity : AppCompatActivity(), ListaProdutosFragment.ListaProdutosFragmentInterface, ProdutoFragment.ProdutoFragmentListener {


    private var mAuth: FirebaseAuth? = null
    private lateinit var fragmentManager: FragmentManager
    private var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produtos)

        fragmentManager = supportFragmentManager

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mAuth = FirebaseAuth.getInstance()

        user = FirebaseAuth.getInstance().currentUser

        val intent = intent
        val idCategoria = intent.getIntExtra("idCategoria", 0)
        val nomeCategoria = intent.getStringExtra("nomeCategoria")
        val nomeProduto = intent.getStringExtra("nomeProduto")

        if (intent.hasExtra("id")) {
            val idProduto = intent.getIntExtra("id", 0)
            val categoria = intent.getIntExtra("categoria", 0)


            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction?.replace(R.id.produtosFrameLayout, ProdutoFragment.newInstance(idProduto, categoria, user?.uid), ProdutoFragment.TAG)
            fragmentTransaction?.commit()
        } else {
            if (savedInstanceState == null) {
                val listaProdutosFragment = fragmentManager.findFragmentByTag(ListaProdutosFragment.TAG)

                if (listaProdutosFragment == null) {
                    val fragmentTransaction = fragmentManager.beginTransaction()
                    fragmentTransaction.add(R.id.produtosFrameLayout, ListaProdutosFragment.newInstance(idCategoria, nomeProduto), ListaProdutosFragment.TAG)
                    fragmentTransaction.commit()
                }

            }

            title = nomeCategoria

        }

    }

    override fun onItemClick(id: Int, categoria: Int) {
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction?.replace(R.id.produtosFrameLayout, ProdutoFragment.newInstance(id, categoria, user!!.uid), ProdutoFragment.TAG)?.addToBackStack(null)
        fragmentTransaction?.commit()
    }


    override fun onResume() {
        super.onResume()
        user = mAuth?.currentUser

        if (intent.hasExtra("id")) {
            val idProduto = intent.getIntExtra("id", 0)
            var categoria = intent.getIntExtra("categoria", 0)

            if(intent.hasExtra("idCategoria")){
                categoria = intent.getIntExtra("idCategoria", 0)
            }

            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction?.replace(R.id.produtosFrameLayout, ProdutoFragment.newInstance(idProduto, categoria, user?.uid), ProdutoFragment.TAG)
            fragmentTransaction?.commit()
        }

    }

    override fun onSelecionarProduto(id: Int, categoria : Int) {
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction?.replace(R.id.produtosFrameLayout, ProdutoFragment.newInstance(id, categoria, user?.uid), ProdutoFragment.TAG)?.addToBackStack(null)
        fragmentTransaction?.commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (intent.hasExtra("id")) {
            super.onBackPressed()
        } else {
            if (fragmentManager.backStackEntryCount > 0) {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            } else {
                super.onBackPressed()
            }
        }

    }

    override fun autenticar(id : Int) {
        intent.putExtra("id", id)
        val newIntent = Intent(this, LoginActivity::class.java)
        newIntent.putExtra("route", "login")
        startActivity(newIntent)
    }

    override fun onTerminoCompra() {
        Toast.makeText(this, "Compra realizada com sucesso!", Toast.LENGTH_LONG).show()
        finish()
    }

}
