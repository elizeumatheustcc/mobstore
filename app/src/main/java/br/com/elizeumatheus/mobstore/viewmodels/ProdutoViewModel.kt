package br.com.elizeumatheus.mobstore.viewmodels

import android.arch.lifecycle.ViewModel
import android.os.Parcel
import android.os.Parcelable

class ProdutoViewModel() : ViewModel(), Parcelable {

    var autor: String = ""
    var categoria: Int = 0
    var id: Int = 0
    var imagem : String = ""
    var marca: String = ""
    var nome: String = ""
    var preco: String = ""
    var visualizacoes : Int = 0

    constructor(parcel: Parcel) : this() {
        autor = parcel.readString()
        categoria = parcel.readInt()
        id = parcel.readInt()
        imagem = parcel.readString()
        marca = parcel.readString()
        nome = parcel.readString()
        preco = parcel.readString()
        visualizacoes = parcel.readInt()
    }

    constructor(autor: String, categoria: Int, id: Int, imagem: String, marca: String, nome: String, preco: String, visualizacoes : Int) : this() {
        this.autor = autor
        this.categoria = categoria
        this.id = id
        this.imagem = imagem
        this.marca = marca
        this.nome = nome
        this.preco = preco
        this.visualizacoes = visualizacoes
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(autor)
        parcel.writeInt(categoria)
        parcel.writeInt(id)
        parcel.writeString(imagem)
        parcel.writeString(marca)
        parcel.writeString(nome)
        parcel.writeString(preco)
        parcel.writeInt(visualizacoes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProdutoViewModel> {
        override fun createFromParcel(parcel: Parcel): ProdutoViewModel {
            return ProdutoViewModel(parcel)
        }

        override fun newArray(size: Int): Array<ProdutoViewModel?> {
            return arrayOfNulls(size)
        }
    }


}