package br.com.elizeumatheus.mobstore.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import br.com.elizeumatheus.mobstore.MainActivity
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.adapter.ParentItemAdapter
import br.com.elizeumatheus.mobstore.utils.AdapterCallback
import br.com.elizeumatheus.mobstore.viewmodels.ItemViewModel
import br.com.elizeumatheus.mobstore.viewmodels.KItemViewModel
import br.com.elizeumatheus.mobstore.viewmodels.ParentItemViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*


class MainContentFragment : Fragment(), AdapterCallback, SwipeRefreshLayout.OnRefreshListener {

    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var myRef: DatabaseReference = database.reference

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout : SwipeRefreshLayout? = null

    private var fragmentInterfaceListener: MainContentListener? = null

    private var mAuth: FirebaseAuth? = null
    private var mUser: FirebaseUser? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val layout = inflater.inflate(R.layout.fragment_main, container, false)

        recyclerView = layout.findViewById(R.id.mainContentFragmentRecyclerView)
        progressBar = layout.findViewById(R.id.mainContentFragmentProgressBar)

        recyclerView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE

        swipeRefreshLayout = layout.findViewById(R.id.mainContentFragmentSwipeRefreshLayout)
        swipeRefreshLayout?.setOnRefreshListener(this)

        mAuth = FirebaseAuth.getInstance()
        mUser = FirebaseAuth.getInstance().currentUser

        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView?.layoutManager = linearLayoutManager
        recyclerView?.setHasFixedSize(true)

        recyclerView?.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                swipeRefreshLayout?.isEnabled = linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0
            }
        })

        buscarProdutos()

        return layout
    }

    override fun onItemClick(id: Int, categoria: Int) {
        fragmentInterfaceListener?.onItemClick(id, categoria)
    }

    private fun buscarProdutos() {

        val query: Query = myRef.child("produtos")

        recyclerView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE

        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {

                Toast.makeText(context, "Falha na conexão. Por favor, tente novamente", Toast.LENGTH_LONG).show()

                progressBar?.visibility = View.GONE
                recyclerView?.visibility = View.VISIBLE

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.exists()) {

                    val kitems: MutableList<KItemViewModel> = mutableListOf()

                    for (postSnapshot: DataSnapshot in dataSnapshot.children) {
                        val comprados: MutableList<String> = mutableListOf()
                        if (postSnapshot.hasChild("comprados")) {

                            for (postChild: DataSnapshot in postSnapshot.child("comprados").children) {
                                val comprado = postChild.value as String
                                comprados.add(comprado)
                            }
                        }

                        val produto: ItemViewModel = postSnapshot.getValue(ItemViewModel::class.java)!!

                        val kitem = KItemViewModel(produto.id, produto.nome, produto.imagem, produto.preco, produto.categoria, produto.visualizacoes, comprados)
                        kitems.add(kitem)

                    }

                    val items = recomendacaoProdutos(kitems)

                    val adapter = ParentItemAdapter(this@MainContentFragment, items)
                    recyclerView?.adapter = adapter

                    adapter.notifyDataSetChanged()

                    if(swipeRefreshLayout?.isRefreshing!!){
                        swipeRefreshLayout?.isRefreshing = false
                    }

                    recyclerView?.visibility = View.VISIBLE
                    progressBar?.visibility = View.GONE

                }
            }
        })

    }

    override fun onRefresh() {
        buscarProdutos()
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivity) {
            fragmentInterfaceListener = context
        }
    }

    private fun recomendacaoProdutos(items: MutableList<KItemViewModel>): MutableList<ParentItemViewModel> {
        val uid = mUser?.uid

        val maisComprados = items.filter { it.comprados.size > 0 }.sortedWith(compareBy({ it.comprados.size })).takeLast(5)
        val maisVisualizados = items.filter { it.visualizacoes >= 0 }.sortedByDescending { it.visualizacoes }.take(5)

        val compradosPeloUsuario = items.filter { it.comprados.size > 0 && it.comprados.contains(uid) }

        var recomendados = items.filter { it.comprados.size > 0 }.shuffled().take(5)

        if (compradosPeloUsuario.count() > 0) {
            recomendados = items.filter { compradosPeloUsuario.map { it.categoria }.contains(it.categoria) }.shuffled().take(5)
        }


        val lista: MutableList<ParentItemViewModel> = mutableListOf()

        lista.add(ParentItemViewModel("Mais comprados", 0, maisComprados))
        lista.add(ParentItemViewModel("Mais visualizados", 1, maisVisualizados))
        lista.add(ParentItemViewModel("Recomendados para você", 2, recomendados))

        return lista
    }

    interface MainContentListener {
        fun onItemClick(id: Int, categoria: Int)
    }

    companion object {
        const val TAG = "MAIN_CONTENT_FRAGMENT"
    }

}