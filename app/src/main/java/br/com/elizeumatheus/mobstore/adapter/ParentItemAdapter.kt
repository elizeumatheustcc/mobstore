package br.com.elizeumatheus.mobstore.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.fragments.ProdutoFragment
import br.com.elizeumatheus.mobstore.utils.AdapterCallback
import br.com.elizeumatheus.mobstore.utils.RecyclerItemClickListener
import br.com.elizeumatheus.mobstore.viewmodels.ParentItemViewModel

class ParentItemAdapter(var fragment: Fragment, var items: MutableList<ParentItemViewModel>) : RecyclerView.Adapter<ParentItemAdapter.ViewHolder>() {

    private var listener : AdapterCallback = fragment as AdapterCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentItemAdapter.ViewHolder {
        val itemLayoutView = LayoutInflater.from(parent.context).inflate(R.layout.parent_item_layout, parent, false)

        return ViewHolder(itemLayoutView)
    }

    override fun getItemCount(): Int {
       return  items.size
    }

    override fun onBindViewHolder(holder: ParentItemAdapter.ViewHolder, position: Int) {

        val item = items[position]

        holder.header.text = item.titulo

        holder.recyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(holder.recyclerView.context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        holder.recyclerView.layoutManager = layoutManager

        val itemAdapter = ItemAdapter(holder.recyclerView.context, item.items)
        holder.recyclerView.adapter = itemAdapter


        holder.recyclerView.addOnItemTouchListener(RecyclerItemClickListener(fragment.context!!, holder.recyclerView, object : RecyclerItemClickListener.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val id = item.items[position].id
                val categoria = item.items[position].categoria
                listener.onItemClick(id, categoria)
            }

            override fun onLongItemClick(view: View, position: Int) {

            }
        }))

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var header: TextView = itemView.findViewById(R.id.parentItemTextViewHeader)
        var recyclerView: RecyclerView = itemView.findViewById(R.id.parentItemRecyclerView)

    }


}