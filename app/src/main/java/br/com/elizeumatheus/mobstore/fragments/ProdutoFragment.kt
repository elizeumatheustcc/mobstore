package br.com.elizeumatheus.mobstore.fragments

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.Toast
import br.com.elizeumatheus.mobstore.ProdutosActivity
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.adapter.ProdutoAdapter
import br.com.elizeumatheus.mobstore.databinding.FragmentProdutoBinding
import br.com.elizeumatheus.mobstore.utils.AdapterCallback
import br.com.elizeumatheus.mobstore.viewmodels.ProdutoViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ProdutoFragment : Fragment(), AdapterCallback {


    private var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    private var myRef: DatabaseReference = database.reference
    private var mAuth: FirebaseAuth? = null
    private lateinit var viewmodel: ProdutoViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null

    private var listener: ProdutoFragmentListener? = null

    private var uid : String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentProdutoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_produto, container, false)
        viewmodel = ViewModelProviders.of(this).get(ProdutoViewModel::class.java)

        val id = arguments?.getInt("id")
        val categoria = arguments?.getInt("categoria")
        uid = arguments?.getString("uid")

        mAuth = FirebaseAuth.getInstance()

        progressBar = binding.root.findViewById(R.id.produtoProgressBar)
        recyclerView = binding.root.findViewById(R.id.produtoRecyclerView)

        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView?.layoutManager = layoutManager

        progressBar?.visibility = View.VISIBLE
        recyclerView?.visibility = View.GONE

        val query: Query? = myRef.child("produtos").orderByChild("categoria").equalTo(categoria!!.toDouble())

        query?.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                Toast.makeText(context, "Falha na conexão. Por favor, tente novamente", Toast.LENGTH_LONG).show()

                progressBar?.visibility = View.GONE
                recyclerView?.visibility = View.VISIBLE
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {

                    var listaProdutos = mutableListOf<ProdutoViewModel>()
                    var key = ""

                    for (postSnapshot: DataSnapshot in dataSnapshot.children) {
                        val produto: ProdutoViewModel = postSnapshot.getValue(ProdutoViewModel::class.java)!!

                        if (produto.id == id) {
                            key = postSnapshot.key
                        }

                        listaProdutos.add(produto)
                    }

                    val produtoAtual = listaProdutos.find { it.id == id }
                    listaProdutos.remove(produtoAtual)
                    listaProdutos = listaProdutos.shuffled().take(5).toMutableList()

                    val items = mutableListOf<Any>()
                    items.add(produtoAtual!!)
                    items.add(listaProdutos)

                    val adapter = ProdutoAdapter(this@ProdutoFragment, items)
                    recyclerView?.adapter = adapter

                    adapter.notifyDataSetChanged()

                    var visualizacoes = produtoAtual.visualizacoes
                    visualizacoes++

                    val ref = myRef.child("produtos").child(key)
                    ref.child("visualizacoes").setValue(visualizacoes)


                }

                progressBar?.visibility = View.GONE
                recyclerView?.visibility = View.VISIBLE
            }
        })



        return binding.root
    }

    override fun onItemClick(id: Int, categoria: Int) {
        listener?.onItemClick(id, categoria)
    }

    override fun onComprar(id: Int) {
        progressBar?.visibility = View.VISIBLE
        recyclerView?.visibility = View.GONE

        if(uid == null){
            listener?.autenticar(id)
        }else{
            val queryComprar = myRef.child("produtos").orderByChild("id").equalTo(id.toDouble())

            queryComprar?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {

                    Toast.makeText(context, "Falha na conexão. Por favor, tente novamente", Toast.LENGTH_LONG).show()

                    progressBar?.visibility = View.GONE
                    recyclerView?.visibility = View.VISIBLE
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    if (dataSnapshot.exists()) {
                        for (postSnapshot: DataSnapshot in dataSnapshot.children) {
                            val ref = myRef.child("produtos").child(postSnapshot.key)
                            ref.child("comprados").push().setValue(uid)

                            progressBar?.visibility = View.GONE
                            recyclerView?.visibility = View.VISIBLE

                            listener?.onTerminoCompra()
                        }

                    }

                }
            })
        }

    }

    interface ProdutoFragmentListener {
        fun onTerminoCompra()
        fun onItemClick(id: Int, categoria: Int)
        fun autenticar(id : Int)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is ProdutosActivity) {
            listener = context
        }
    }


    companion object {
        const val TAG = "PRODUTO_FRAGMENT"

        fun newInstance(id: Int, categoria: Int, uid: String?): ProdutoFragment {
            val fragment = ProdutoFragment()

            val args = Bundle()
            args.putInt("id", id)
            args.putString("uid", uid)
            args.putInt("categoria", categoria)

            fragment.arguments = args

            return fragment
        }

    }

}