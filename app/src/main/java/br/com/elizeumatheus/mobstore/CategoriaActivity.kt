package br.com.elizeumatheus.mobstore

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import br.com.elizeumatheus.mobstore.adapter.CategoriaAdapter
import br.com.elizeumatheus.mobstore.viewmodels.CategoriaViewModel
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.acitivity_categoria.*

class CategoriaActivity : AppCompatActivity(){

    private var database : FirebaseDatabase = FirebaseDatabase.getInstance()
    private var myRef : DatabaseReference = database.getReference("categorias")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acitivity_categoria)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val listView = categoriaListView
        val progressBar = categoriaProgressBar

        listView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE

        myRef.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val categorias : MutableList<CategoriaViewModel> = mutableListOf()
                for(postSnapshot : DataSnapshot in dataSnapshot.children){
                    val categoria : CategoriaViewModel = postSnapshot.getValue(CategoriaViewModel::class.java)!!
                    categorias.add(categoria)
                }

                categorias.sortBy { it.Descricao }
                val adapter = CategoriaAdapter(this@CategoriaActivity, categorias)
                listView.adapter = adapter

                adapter.notifyDataSetChanged()

                listView?.visibility = View.VISIBLE
                progressBar?.visibility = View.GONE

            }
        })

        listView.setOnItemClickListener{
            adapterView: AdapterView<*>, _: View?, position: Int, _: Long ->

            val categoria = adapterView.adapter.getItem(position) as CategoriaViewModel

            val intent = Intent(this, ProdutosActivity::class.java)
            intent.putExtra("idCategoria", categoria.Id)
            intent.putExtra("nomeCategoria", categoria.Descricao)
            startActivity(intent)

        }




    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}