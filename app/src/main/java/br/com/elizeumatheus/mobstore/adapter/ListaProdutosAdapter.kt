package br.com.elizeumatheus.mobstore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.viewmodels.ListaProdutosViewModel
import com.bumptech.glide.Glide

class ListaProdutosAdapter(private var context: Context, private var produtos : MutableList<ListaProdutosViewModel>) : BaseAdapter(){

    private class ViewHolder(view : View?){
        var nome : TextView? = null
        var preco : TextView? = null
        var imagem : ImageView? = null

        init {
            this.nome = view?.findViewById(R.id.listaProdutosItemTextViewDescricao)
            this.preco = view?.findViewById(R.id.listaProdutosItemTextViewPreco)
            this.imagem = view?.findViewById(R.id.listaProdutosItemImageViewProduto)
        }
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view : View?
        val viewHolder : ViewHolder

        if(convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.lista_produto_item, parent, false)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        }else{
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        val produto = produtos[position]

        viewHolder.nome?.text = produto.nome
        viewHolder.preco?.text = produto.preco

        Glide.with(context).load(produto.imagem).into(viewHolder.imagem!!)

        return view as View
    }

    override fun getItem(position: Int): Any {
        return produtos[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return produtos.size
    }

}