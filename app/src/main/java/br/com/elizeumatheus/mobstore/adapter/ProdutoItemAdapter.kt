package br.com.elizeumatheus.mobstore.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.elizeumatheus.mobstore.R
import br.com.elizeumatheus.mobstore.viewmodels.ProdutoViewModel
import com.bumptech.glide.Glide

class ProdutoItemAdapter(var context: Context, var items : List<ProdutoViewModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemViewLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)

        return ViewHolder(itemViewLayout)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        val viewHolder = holder as ViewHolder

        viewHolder.titulo.text = item.nome
        viewHolder.preco.text = item.preco

        Glide.with(context).load(item.imagem).into(holder.imagem)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var titulo: TextView = itemView.findViewById(R.id.itemTextViewTitulo)
        var preco: TextView = itemView.findViewById(R.id.itemTextViewPreco)
        var imagem: ImageView = itemView.findViewById(R.id.itemImageViewImagem)

    }

}