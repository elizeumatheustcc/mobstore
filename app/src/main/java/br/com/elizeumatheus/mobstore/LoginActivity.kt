package br.com.elizeumatheus.mobstore

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import br.com.elizeumatheus.mobstore.adapter.ViewPagerAdapter
import br.com.elizeumatheus.mobstore.fragments.CadastroFragment
import br.com.elizeumatheus.mobstore.fragments.LoginFragment
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginFragment.LoginFragmentInterface, CadastroFragment.CadastroFragmentInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 0F

        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        val viewPager = loginViewPager
        loginViewPager.adapter = viewPagerAdapter

        val tabLayout = loginTabLayout
        tabLayout.setupWithViewPager(viewPager)

    }

    override fun onTerminoLogin() {
        if(intent.hasExtra("route")){
            finish()
        }else{
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    override fun onTerminoCadastro(nome : String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra("nome", nome)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        this.onBackPressed()
        return true
    }

}
