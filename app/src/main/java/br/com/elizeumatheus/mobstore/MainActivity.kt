package br.com.elizeumatheus.mobstore

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import br.com.elizeumatheus.mobstore.fragments.MainContentFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.appbar_layout.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainContentFragment.MainContentListener {


    private var mAuth : FirebaseAuth? = null
    private var navigationView : NavigationView? = null
    private var user : FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        mAuth = FirebaseAuth.getInstance()

        user = FirebaseAuth.getInstance().currentUser

        val toggle = ActionBarDrawerToggle(this, mainActivityDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        mainActivityDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navigationView = mainActivityNavigationView

        navigationView?.setNavigationItemSelectedListener(this)

        if(user == null) {
            navigationView?.inflateHeaderView(R.layout.navigation_menu_header_dislogged)
            val header = navigationView?.getHeaderView(0)
            val textView = header!!.findViewById<TextView>(R.id.navigationMenuHeaderDisloggedTextViewLogin)
            textView.setOnClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }else{

            navigationView?.inflateHeaderView(R.layout.navigation_menu_header_logged)
            val header = navigationView?.getHeaderView(0)
            val nome = header!!.findViewById<TextView>(R.id.navigationMenuHeaderLoggedTextViewNome)
            val email = header.findViewById<TextView>(R.id.navigationMenuHeaderLoggedTextViewEmail)

            email.text = user?.email

            val intent = intent
            if(intent.hasExtra("nome")){

                val extra = intent.getStringExtra("nome")

                nome.text = extra

                val info = UserProfileChangeRequest.Builder()
                        .setDisplayName(extra)
                        .build()

                user?.updateProfile(info)!!.addOnCompleteListener{
                    task ->

                    if(task.isSuccessful){
                        nome.text = user?.displayName
                    }
                }

            }else{
                nome.text = user?.displayName
            }


        }

        if(savedInstanceState == null){
            val mainContentFragment = supportFragmentManager?.findFragmentByTag(MainContentFragment.TAG)
            if(mainContentFragment == null){
                val fragmentTransaction = supportFragmentManager?.beginTransaction()
                fragmentTransaction?.add(R.id.frameLayoutMainContent, MainContentFragment(), MainContentFragment.TAG)
                fragmentTransaction?.commit()
            }
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.navigationMenuItemHome ->{
                title = getString(R.string.app_name)
                item.isChecked = false
            }
            R.id.navigationMenuItemCategoria ->{
                title = item.title
                item.isChecked = false
                startActivity(Intent(this, CategoriaActivity::class.java))
            }
            R.id.navigationMenuItemSair ->{
                item.isChecked = false
                mAuth?.signOut()
                recreate()
            }
        }
        uncheckAllMenuItems(navigationView!!)
        mainActivityDrawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if(mainActivityDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mainActivityDrawerLayout.closeDrawer(GravityCompat.START)
        }else{
            super.onBackPressed()
        }
    }

    override fun onItemClick(id: Int, categoria : Int) {

        val intent = Intent(this, ProdutosActivity::class.java)
        intent.putExtra("id", id)
        intent.putExtra("categoria", categoria)
        startActivity(intent)

    }

    override fun onResume() {
        super.onResume()
        user = mAuth?.currentUser

        if(user != null){
            navigationView?.removeHeaderView(navigationView?.getHeaderView(0)!!)
            navigationView?.inflateHeaderView(R.layout.navigation_menu_header_logged)
            val header = navigationView?.getHeaderView(0)
            val nome = header!!.findViewById<TextView>(R.id.navigationMenuHeaderLoggedTextViewNome)
            val email = header.findViewById<TextView>(R.id.navigationMenuHeaderLoggedTextViewEmail)

            email.text = user?.email

            val intent = intent
            if(intent.hasExtra("nome")){

                val extra = intent.getStringExtra("nome")

                nome.text = extra

                val info = UserProfileChangeRequest.Builder()
                        .setDisplayName(extra)
                        .build()

                user?.updateProfile(info)!!.addOnCompleteListener{
                    task ->

                    if(task.isSuccessful){
                        nome.text = user?.displayName
                    }
                }

            }else{
                nome.text = user?.displayName
            }
        }
    }

    private fun uncheckAllMenuItems(navigationView: NavigationView) {
        val menu = navigationView.menu
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item.hasSubMenu()) {
                val subMenu = item.subMenu
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    subMenuItem.isChecked = false
                }
            } else {
                item.isChecked = false
            }
        }
    }

}
